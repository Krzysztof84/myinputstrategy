import java.util.Scanner;

/**
 * Created by amen on 8/21/17.
 */
public class StrategyStdIn implements IInputStrategy {
    Scanner scanner = new Scanner(System.in);
    public StrategyStdIn() {
    }

    @Override
    public int getInt() {
        int i = scanner.nextInt();
        return i;
    }

    @Override
    public double getDouble() {
        double u = scanner.nextDouble();
        return u;
    }

    @Override
    public String getString() {
        String o = scanner.nextLine();
        return o;
    }
}
